module gitlab.com/smartgeosystem/geokaskad/geokaskad-server/module-upload

go 1.16

require (
	github.com/c2fo/vfs/v5 v5.5.7
	github.com/google/uuid v1.1.2
	github.com/gorilla/mux v1.7.3
	github.com/hashicorp/go-version v1.3.0
	github.com/sirupsen/logrus v1.8.1 // indirect
	gitlab.com/smartgeosystem/geokaskad/geokaskad-server/core v0.0.0-20210910224227-0c707568c88c
	gorm.io/gorm v1.21.10
)

//replace gitlab.com/smartgeosystem/geokaskad/geokaskad-server/core v0.0.0-20210910224227-0c707568c88c => ../core
