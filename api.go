package module_upload

import (
	"fmt"
	"github.com/c2fo/vfs/v5"
	"github.com/google/uuid"
	"gitlab.com/smartgeosystem/geokaskad/geokaskad-server/core/http_server"
	"gorm.io/gorm"
	"io"
	"net/http"
	"time"
)

func handleUploadFile(locationGetter func() (vfs.Location, error), connection *gorm.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		err := r.ParseMultipartForm(1024 * 1024) // 1Mb in memory
		if err != nil {
			errText := "Error on ParseMultipartForm: " + err.Error()
			log.Errorln(errText)
			resp := http_server.CommonResponse{Status: http.StatusBadRequest, Error: errText}
			http_server.WriteCommonJsonResponse(w, resp)
			return
		}

		uploadedFile, handler, err := r.FormFile("file")
		if err != nil {
			errText := "Error on get 'file': " + err.Error()
			log.Errorln(errText)
			resp := http_server.CommonResponse{Status: http.StatusBadRequest, Error: errText}
			http_server.WriteCommonJsonResponse(w, resp)
			return
		}
		defer uploadedFile.Close()
		log.Debugf("Uploaded file: %s \tSize: %d \tMime: %s", handler.Filename, handler.Size, handler.Header)

		dt := time.Now()
		fileUuid := uuid.New()
		filePath := fmt.Sprintf("%s/%s", dt.Format("20060102"), fileUuid.String())

		location, err := locationGetter()
		if err != nil {
			errText := "Error on connect to file storage: " + err.Error()
			log.Errorln(errText)
			resp := http_server.CommonResponse{Status: http.StatusInternalServerError, Error: errText}
			http_server.WriteCommonJsonResponse(w, resp)
			return
		}

		targetFile, err := location.NewFile(filePath)
		if err != nil {
			errText := "Error on create new file on storage: " + err.Error()
			log.Errorln(errText)
			resp := http_server.CommonResponse{Status: http.StatusInternalServerError, Error: errText}
			http_server.WriteCommonJsonResponse(w, resp)
			return
		}

		_, err = io.Copy(targetFile, uploadedFile)
		if err != nil {
			errText := "Error on copy file content: " + err.Error()
			log.Errorln(errText)
			resp := http_server.CommonResponse{Status: http.StatusInternalServerError, Error: errText}
			http_server.WriteCommonJsonResponse(w, resp)
			return
		}

		err = targetFile.Close()
		if err != nil {
			errText := "Error on write file content: " + err.Error()
			log.Errorln(errText)
			resp := http_server.CommonResponse{Status: http.StatusInternalServerError, Error: errText}
			http_server.WriteCommonJsonResponse(w, resp)
			return
		}

		record := GeokaskadUpload{
			ID:       fileUuid.String(),
			UploadDt: dt,
			Name:     handler.Filename,
			Size:     handler.Size,
			MimeType: handler.Header.Get("Content-Type"),
			Path:     filePath,
		}
		result := connection.Create(&record)
		if result.Error != nil {
			errText := "Error on save metadata: " + result.Error.Error()
			log.Errorln(errText)
			resp := http_server.CommonResponse{Status: http.StatusInternalServerError, Error: errText}
			http_server.WriteCommonJsonResponse(w, resp)
			// Try to remove saved file
			if err := targetFile.Close(); err != nil {
				log.Errorf("Error on delete uploaded file: %s", filePath)
			}
			if err := targetFile.Delete(); err != nil {
				log.Errorf("Error on delete uploaded file: %s", filePath)
			}
			return
		}

		resp := http_server.CommonResponse{Status: http.StatusOK, Data: UploadInfo{UploadId: fileUuid}}
		http_server.WriteCommonJsonResponse(w, resp)
		return
	}
}
