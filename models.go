package module_upload

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"time"
)

type UploadInfo struct {
	UploadId uuid.UUID `json:"upload_id"`
}

type GeokaskadUpload struct {
	ID       string    `gorm:"index:uploads_id_idx;size:254;not null;"`
	UploadDt time.Time `gorm:"not null;"`
	Name     string    `gorm:"size:2048;not null;"`
	Size     int64     `gorm:"not null;"`
	MimeType string    `gorm:"size:256;not null;"`
	Path     string    `gorm:"size:4096;not null;"`
}

func (u *GeokaskadUpload) BeforeCreate(tx *gorm.DB) (err error) {
	if u.ID == "" {
		u.ID = uuid.New().String()
	}
	if u.UploadDt.IsZero() {
		u.UploadDt = time.Now()
	}
	return
}
