package module_upload

import (
	"github.com/c2fo/vfs/v5"
)

type UploadConfig struct {
	URI      string
	Settings vfs.Options
}

func GetDefaults() UploadConfig {
	return UploadConfig{
		URI:      "file:///tmp/geokaskad/uploads",
		Settings: nil,
	}
}
