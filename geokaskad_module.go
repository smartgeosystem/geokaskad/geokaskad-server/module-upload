package module_upload

import (
	"github.com/c2fo/vfs/v5"
	"github.com/c2fo/vfs/v5/vfssimple"
	"github.com/gorilla/mux"
	"github.com/hashicorp/go-version"
	"gitlab.com/smartgeosystem/geokaskad/geokaskad-server/core/config"
	"gitlab.com/smartgeosystem/geokaskad/geokaskad-server/core/logger"
	"gitlab.com/smartgeosystem/geokaskad/geokaskad-server/core/metadata"
	"gorm.io/gorm"
	"net/http"
	"sync"
)

//https://github.com/C2FO/vfs
const GeokaskadModuleId = "upload"

var log = logger.GetLogger(GeokaskadModuleId)

type GeokaskadModule struct {
	config          *UploadConfig
	metadataStorage *metadata.GormMetadataStorage
	connection      *gorm.DB
	uploadLocation  vfs.Location
	sync.Mutex
}

func (g GeokaskadModule) ID() string {
	return GeokaskadModuleId
}

func (g GeokaskadModule) Description() string {
	return "Upload module. Allow upload any resources on the server."
}

func (g GeokaskadModule) Version() version.Version {
	v, _ := version.NewVersion(Version)
	return *v
}

func (g GeokaskadModule) CompatCoreVersion() (version.Version, version.Version) {
	vMin, _ := version.NewVersion(MinCoreVersion)
	vMax, _ := version.NewVersion(MaxCoreVersion)
	return *vMin, *vMax
}

func (g GeokaskadModule) CompatMetadataStoreTypes() []metadata.MetadataStorageType {
	return []metadata.MetadataStorageType{metadata.MetadataStorageTypeGorm}
}

func (g GeokaskadModule) RequiredModules() []string {
	return []string{}
}

func (g GeokaskadModule) GetDefaultConfigValues() (string, interface{}) {
	uploadConfigDefault := GetDefaults()
	return GeokaskadModuleId, uploadConfigDefault
}

func (g *GeokaskadModule) ConfigurateModule(configManager *config.ConfigManager) error {
	uploadConfig := GetDefaults()
	err := configManager.UnmarshalKey(GeokaskadModuleId, &uploadConfig)
	if err != nil {
		log.Errorln("Error on configuration: ", err.Error())
		return err
	}
	g.config = &uploadConfig

	//if strings.HasPrefix(uploadConfig.URI, s3.Scheme) {
	//	// Setup s3 addr and auth
	//	s3Opt := s3.Options{}
	//	err = mapstructure.Decode(uploadConfig.Settings, &s3Opt)
	//	if err != nil {
	//		log.Errorln("Error on configuration: ", err.Error())
	//		return err
	//	}
	//	// Default Client
	//	//s3Auth := s3.NewFileSystem().WithOptions(s3Opt)
	//
	//	// Manual AWS Client
	//	// 	//sseType := "AES256" file.go
	//	s3Config := &aws.Config{
	//		Credentials:      aws_credentials.NewStaticCredentials(s3Opt.AccessKeyID, s3Opt.SecretAccessKey, ""),
	//		Endpoint:         aws.String(s3Opt.Endpoint),
	//		Region:           aws.String("us-east-1"),
	//		DisableSSL:       aws.Bool(true),
	//		S3ForcePathStyle: aws.Bool(true),
	//
	//	}
	//	newSession := aws_session.New(s3Config)
	//	s3Client := aws_s3.New(newSession)
	//	s3Auth := s3.NewFileSystem().WithClient(s3Client)
	//
	//	backend.Register(uploadConfig.URI, s3Auth)
	//	backend.Unregister(s3.Scheme) // OOPS!
	//}

	return nil
}

func (g *GeokaskadModule) MigrateMetadata(storage metadata.MetadataStorage) error {
	g.metadataStorage = storage.(*metadata.GormMetadataStorage)
	g.connection = g.metadataStorage.GetConnection().(*gorm.DB)
	err := g.connection.AutoMigrate(&GeokaskadUpload{})
	if err != nil {
		log.Errorln("Error on migration: ", err.Error())
		return err
	}
	return nil
}

func (g GeokaskadModule) InitRouter(router *mux.Router) error {
	router.HandleFunc("/api/upload", handleUploadFile(g.getUploadLocation, g.connection)).Methods(http.MethodPost)
	return nil
}

func (g GeokaskadModule) RunModule() error {
	//TODO: setup extra params for s3\gs\... providers

	//TODO: try to create Location?

	//TODO: start cron task for auto clean uploads
	return nil
}

func (g GeokaskadModule) StopModule() error {
	//TODO: stop cron task for auto clean uploads
	return nil
}

func (g *GeokaskadModule) getUploadLocation() (vfs.Location, error) {
	// TODO: Or make new connection everytime?
	if g.uploadLocation == nil {
		g.Mutex.Lock()
		defer g.Mutex.Unlock()
		if g.uploadLocation == nil {
			var err error
			g.uploadLocation, err = vfssimple.NewLocation(g.config.URI)
			if err != nil {
				log.Errorln("Error on get upload location:", err)
			}
			return g.uploadLocation, err
		}
	}
	return g.uploadLocation, nil
}
